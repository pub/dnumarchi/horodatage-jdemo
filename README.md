# Exemple d'utilisation d'un service d'horodatage RFC 3161

Ce mini-projet illustre l'utilisation d'un service d'horodatage RFC 3161 en Java.

## Construction de l'application

Ce projet suppose une version Java 8 ou supérieure.

Installer l'outil de construction Apache Maven au besoin puis utiliser les commandes usuelles :

* mvn compile : compilation du projet
* mvn package génération d'une librairie horodatage-jdemo-[version].jar

Le projet intègre le plugin Maven Spring Boot. Le packaging (mvn package) produit également un jar autonome et exécutable (/*fat jar*/) : horodatage-jdemo-[version]-run.jar


## Exécution de l'application

/*Exemple de service d'horodatage gratuit : FreeTSA : https://freetsa.org/tsr*/

La classe de démarrage est *demo.horodatage.TimestampDemo*

Pour démarrer l'application obtenir la syntaxe :

```shell
# Via Maven
mvn spring-boot:run

# Avec le jar produit
java -jar horodatage-jdemo-[version]-run.jar
```

Pour exécuter l'application avec ses arguments

```shell
# Via Maven
mvn spring-boot:run -Dspring-boot.run.arguments="arg1 arg2 ... argN"

# Avec le jar produit
java -jar horodatage-jdemo-[version]-run.jar arg1 arg2 ... argN
```

## Exemples

Horodater le texte "Hello" sur FreeTSA avec aléas 

```shell
# Via Maven
mvn spring-boot:run -Dspring-boot.run.arguments="-u https://freetsa.org/tsr -t Hello -n"

# Avec le jar produit
java -jar horodatage-jdemo-run.jar -u https://freetsa.org/tsr -t Hello -n
```

## Gestion des certificats

### Authentification serveur

Si le certificat racine du service d'horodatage est inconnu du magasin de certificats java (truststore), il doit être transmis dans un truststore spécifique et renseigné au démarrage :

* -Djavax.net.ssl.trustStore=/chemin/vers/truststore
* -Djavax.net.ssl.trustStorePassword=pass_truststore

### Authentification cliente

Ce démonstrateur ne gère pas d'authentification.

Si le service TSA impose une authentification par certificat, il est possible de le passer sous forme d'un fichier PKCS12 à la ligne de commande en transmettant les paramètres usuels Java :

* -Djavax.net.ssl.keyStore=/chemin/vers/fichier.p12
* -Djavax.net.ssl.keyStorePassword=pass_p12
* -Djavax.net.ssl.keyStoreType=pkcs12

~ Fin ~
