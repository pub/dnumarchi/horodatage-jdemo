package demo.horodatage;

/**
 * Exception issue d'un refus de service d'un serveur TSP.
 * 
 * Cette exception contient les raisons du refus de service tel que transmises par le serveur TSP.
 * 
 */
public class TimestampException extends Exception {
    /** identifiant de classe */
    private static final long serialVersionUID = 1L;
    /** code retour (status dans la RFC3161) de la réponse, -1 si non défini */
    private int status = -1;
    /** identifiant précis de l'erreur (failInfo dans RFC3161) */
    private int failInfo = 0;

    /**
     * @param status code retour (status dans la RFC3161) de la réponse, -1 si non défini
     */
    public TimestampException(int status) {
        this.status = status;
    }

    /**
     * @param détail sur l'erreur (statusString dans RFC3161)
     */
    public TimestampException(String statusString) {
        super(statusString);
    }

    /**
     * @param status code retour (status) de la réponse, tel que défini dans la RFC3161, -1 si non défini
     * @param thw erreur parente
     */
    public TimestampException(int status, Throwable thw) {
        super(thw);
        this.status = status;
    }

    /**
     * @param status code retour (status dans la RFC3161) de la réponse, -1 si non défini
     * @param détail sur l'erreur (statusString dans RFC3161)
     */
    public TimestampException(int status, String statusString) {
        super(statusString);
        this.status = status;
    }

    /**
     * @param status code retour (status dans la RFC3161) de la réponse, -1 si non défini
     * @param détail sur l'erreur (statusString dans RFC3161)
     * @param thw erreur parente
     */
    public TimestampException(int status, String statusString, Throwable thw) {
        super(statusString, thw);
        this.status = status;
    }

    /**
     * @param failInfo identifiant précis de l'erreur (failInfo dans RF3161)
     */
    public void setFailInfo(int failInfo) {
        this.failInfo = failInfo;
    }

    /**
     * @return code retour (status dans la RFC3161) de la réponse, -1 si non défini
     */
    public int getStatus() {
        return status;
    }

    /**
     * @return identifiant précis de l'erreur (failInfo dans RF3161)
     */
    public int getFailInfo() {
        return failInfo;
    }

    @Override
    public String toString() {
        return String.format("Erreur d'horodatage ; %d: %s [%d]", status, getMessage(), failInfo);
    }
}
