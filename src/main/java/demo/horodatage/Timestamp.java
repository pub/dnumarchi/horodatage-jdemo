package demo.horodatage;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.Iterator;
import java.util.Random;

import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.tsp.TSPAlgorithms;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampRequest;
import org.bouncycastle.tsp.TimeStampRequestGenerator;
import org.bouncycastle.tsp.TimeStampResponse;
import org.bouncycastle.tsp.TimeStampToken;
import org.bouncycastle.util.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utilitaire d'horodatage selon la RFC 3161
 */
public class Timestamp {
    /** longeur de l'aléa généré */
    private static final int NONCE_LENGTH = 64;
    /** Générateur de nombres pseudo-aléatoires */
    private static final Random RANDOM = new SecureRandom();
    /** identifiant de la politique d'horodatage */
    private String tsuOID;
    /** générer un aleas */
    private boolean withNonce = false;
    /** générer un aleas */
    private boolean withCert = false;
    private static final Logger LOG = LoggerFactory.getLogger(Timestamp.class);

    /**
     * @param withNonce true pour ajouter automatiquement un aleas de 64 bits
     */
    public void setNonce(boolean withNonce) {
        this.withNonce = withNonce;
    }

    /**
     * @param withCert true pour demander le certificat utilisé pour horodater
     */
    public void setCert(boolean withCert) {
        this.withCert = withCert;
    }

    /**
     * @param tsuOID identifiant optionnel de la politique d'horodatage
     */
    public void setOID(String tsuOID) {
        if ((tsuOID == null) || (tsuOID.trim().length() == 0)) {
            this.tsuOID = null;
        } else {
            this.tsuOID = tsuOID;
        }
    }

    /**
     * @param hash empreinte à horodater
     * 
     * @return requête d'horodatageau format ASN.1
     * 
     * @throws IOException Erreur d'encodage ASN.1 de la requête.
     */
    public byte[] buildTSRequest(byte[] hash) throws IOException {
        TimeStampRequestGenerator tspRequestGenerator = new TimeStampRequestGenerator();
        if (tsuOID != null) {
            LOG.info("Ajout OID: {}", tsuOID);
            tspRequestGenerator.setReqPolicy(new ASN1ObjectIdentifier(tsuOID));
        }
        if (withCert) {
            LOG.info("Demande de certificat signataire");
            tspRequestGenerator.setCertReq(true);
        }
        TimeStampRequest tspRequest;
        if (withNonce) {
            BigInteger nonce = new BigInteger(NONCE_LENGTH, RANDOM);
            LOG.info("Ajout nonce: {}", nonce);
            tspRequest = tspRequestGenerator.generate(TSPAlgorithms.SHA256, hash, nonce);
        } else {
            tspRequest = tspRequestGenerator.generate(TSPAlgorithms.SHA256, hash);

        }

        return tspRequest.getEncoded();
    }

    /**
     * @param timestampURL URL du service d'horodatage
     * @param tsRequest requête d'horodatage au format ASN.1
     * 
     * @return réponse du service d'horodatage, au format ASN.1
     * 
     * @throws IOException erreur de connexion HTTP ou d'encodage ASN.1 de la réponse
     * @throws TimestampException rejet du répondeur TSP
     * @throws TSPException
     */
    public byte[] buildTSResponseFromURL(byte[] tsRequest, String timestampURL) throws IOException, TimestampException {

        // Post HTTP de la requête
        LOG.debug("Ouverture de la connexion, url: {}", timestampURL);
        HttpURLConnection httpCnx = (HttpURLConnection) (new URL(timestampURL)).openConnection();
        httpCnx.setDoOutput(true);
        httpCnx.setDoInput(true);
        httpCnx.setRequestMethod("POST");
        httpCnx.setRequestProperty("Content-type", "application/timestamp-query");
        httpCnx.setRequestProperty("Content-length", String.valueOf(tsRequest.length));

        OutputStream out = httpCnx.getOutputStream();
        out.write(tsRequest);
        out.flush();

        // Lecture de la réponse HTTP = réponse ASN.1
        if (httpCnx.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new IOException(String.format("Requête d'horodatage invalide : %d (%s)", httpCnx.getResponseCode(),
                    httpCnx.getResponseMessage()));
        }
        TimeStampResponse tspResponse;
        try (ASN1InputStream asn1Istr = new ASN1InputStream(httpCnx.getInputStream())) {
            tspResponse = new TimeStampResponse(asn1Istr);
        } catch (TSPException e) {
            throw new IOException("Analyse de la réponse d'horodatage impossible : " + e.getMessage(), e);
        }

        // Vérification de la réponse
        TimestampException tspe = null;
        if (tspResponse.getStatus() != 0) {
            tspe = new TimestampException(tspResponse.getStatus(), tspResponse.getStatusString());
            if (tspResponse.getFailInfo() != null) {
                tspe.setFailInfo(tspResponse.getFailInfo().intValue());
                throw tspe;
            }
        }

        // La réponse est valide : encodage ASN.1
        return tspResponse.getEncoded();
    }

    /**
     * Lecture du certificat de signature de l'unité d'horodatage dans une réponse d'horodatage
     * 
     * @param tsResponse réponse du service d'horodatage, au format ASN.1
     * 
     * @return certificat de l'unité d'horodatage si présent, null sinon.
     * 
     * @throws IOException erreur de lecture de la réponse
     * @throws CertificateExceptionformat de certificat invalide
     */
    public static X509Certificate readCertFromResponse(byte[] tsResponse) throws IOException, CertificateException {
        X509Certificate x509 = null;

        TimeStampResponse tspResponse;
        try {
            tspResponse = new TimeStampResponse(tsResponse);
            TimeStampToken tspToken = tspResponse.getTimeStampToken();
            @SuppressWarnings("rawtypes")
            Store certStore = tspToken.getCertificates();
            @SuppressWarnings("unchecked")
            Iterator<X509CertificateHolder> certIt = certStore.getMatches(tspToken.getSID()).iterator();
            if (certIt.hasNext()) {
                LOG.debug("Parcours des certificats, un seul supposé trouvé");
                X509CertificateHolder certh = certIt.next();
                try (InputStream inStream = new ByteArrayInputStream(certh.getEncoded())) {
                    CertificateFactory cf = CertificateFactory.getInstance("X.509");
                    x509 = (X509Certificate) cf.generateCertificate(inStream);
                    LOG.debug("Certificat trouvé: {}", x509.getSubjectDN());
                }
            } else {
                LOG.debug("Aucun certificat trouvé");
            }
        } catch (TSPException e) {
            throw new IOException("Erreur de lecture de la réponse d'horodatage" + e.getMessage(), e);
        }

        return x509;

    }

    /**
     * Conversion en base 64 de données binaires.
     * 
     * @param data données binaires.
     * @return conversion base 64 des données.
     */
    public static String toBase64(byte[] data) {
        return Base64.getEncoder().encodeToString(data);
    }

    /**
     * Calcul d'une empreinte (hash) sur un texte
     * 
     * @param txt texte sur lequel calculer l'empreinte
     * @param algorithm algorithme d'empreinte à utiliser
     * 
     * @return empreinte sur le texte
     * 
     * @throws NoSuchAlgorithmException algorithme inconnu
     */
    public static byte[] digest(String txt, String algorithm) throws NoSuchAlgorithmException, IOException {
        return digest(txt, Charset.defaultCharset().name(), algorithm);
    }

    /**
     * Calcul d'une empreinte (hash) sur un texte
     * 
     * @param txt texte sur lequel calculer l'empreinte
     * @param charset encodage du texte
     * @param algorithm algorithme d'empreinte à utiliser
     * 
     * @return empreinte sur le texte
     * 
     * @throws NoSuchAlgorithmException algorithme inconnu
     */
    public static byte[] digest(String txt, String charset, String algorithm)
            throws NoSuchAlgorithmException, IOException {
        return digest(txt.getBytes(charset), algorithm);
    }

    /**
     * Calcul d'une empreinte (hash) sur un fichier
     * 
     * @param file fichier sur lequel calculer l'empreinte
     * @param algorithm algorithme d'empreinte à utiliser
     * 
     * @return empreinte sur le fichier
     * 
     * @throws NoSuchAlgorithmException algorithme inconnu
     */
    public static byte[] digest(File file, String algorithm) throws NoSuchAlgorithmException, IOException {
        MessageDigest md = MessageDigest.getInstance(algorithm);
        try (FileInputStream instr = new FileInputStream(file)) {
            byte[] data = new byte[1024];
            int read = 0;
            while ((read = instr.read(data)) != -1) {
                md.update(data, 0, read);
            }
            return md.digest();
        }
    }

    /**
     * Calcul d'une empreinte (hash) sur de la donnée binaire
     * 
     * @param data donnée sur laquelle calculer l'empreinte
     * @param algorithm algorithme d'empreinte à utiliser
     * 
     * @return empreinte sur la donnée
     * 
     * @throws NoSuchAlgorithmException algorithme inconnu
     */
    private static byte[] digest(byte[] data, String algorithm) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance(algorithm);
        return md.digest(data);
    }
}