package demo.horodatage;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.concurrent.Callable;

import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampRequest;
import org.bouncycastle.tsp.TimeStampResponse;
import org.bouncycastle.tsp.TimeStampToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import picocli.CommandLine;
import picocli.CommandLine.ArgGroup;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

/**
 * Classe de test d'un service d'horodatage (RFC 3161).
 * 
 */
@Command(name = "TimestampDemo", version = "1.0", description = "Demonstration d'appel à un service d'horodatage compatible RFC3161")
public class TimestampDemo implements Callable<Integer> {
    @Option(names = { "-o", "--oid" }, description = "OID politique horodatage")
    private String oid;
    @Option(names = { "-u", "--url" }, required = true, description = "URL Service TSU")
    private String url;
    @Option(names = { "-c", "--cert" }, description = "Demander le certificat avec le jeton")
    private boolean withCert = false;
    @Option(names = { "-n", "--nonce" }, description = "Creer un aleas de 64 bits")
    private boolean withNonce = false;
    @ArgGroup(exclusive = true, multiplicity = "1")
    private Data data;
    @Option(names = {"-h", "--help"}, usageHelp = true, description = "Afficher la syntaxe")
    boolean usageHelpRequested;
    private static final Logger LOG = LoggerFactory.getLogger(TimestampDemo.class);

    static class Data {
        @Option(names = { "-f", "--fichier" }, description = "Fichier a horodater")
        private File file;
        @Option(names = { "-t", "--text" }, description = "Texte a horodater")
        private String text;
    }

    @Override
    public Integer call() throws Exception {
        /*
         * Construction de la requête d'horodatage
         */
        Timestamp tsp = new Timestamp();
        tsp.setOID(oid);
        tsp.setCert(withCert);
        tsp.setNonce(withNonce);

        byte[] hash = null;
        if (data.file != null) {
            LOG.debug("Calcul d'empreinte du contenu du fichier: {}", data.file.getPath());
            hash = Timestamp.digest(data.file, "SHA-256");
        } else {
            LOG.debug("Calcul d'empreinte du texte: {}", data.text);
            hash = Timestamp.digest(data.text, "SHA-256");
        }
        LOG.info("Hash calculé: {}", hash == null ? "" : Timestamp.toBase64(hash));

        byte[] tsReq = tsp.buildTSRequest(hash);
        TimeStampRequest tspRequest = new TimeStampRequest(tsReq);

        /*
         * Soumission de la requête
         */
        byte[] tsResp = tsp.buildTSResponseFromURL(tsReq, url);
        TimeStampResponse tspResponse = new TimeStampResponse(tsResp);

        /*
         * Lecture de la réponse
         */
        TimeStampToken tspToken = tspResponse.getTimeStampToken();
        // Lecture du certificat de l'unité d'horodatage
        X509Certificate x509 = Timestamp.readCertFromResponse(tsResp);

        /*
         * Validation de la réponse
         */
        try {
            tspResponse.validate(tspRequest);
        } catch (TSPException e) {
            LOG.error("Réponse TSP,invalide : {}", e.getMessage());
        }
        LOG.info("Réponse TSP valide");

        /*
         * Informations sur le jeton
         */
        LOG.info("Date  : {}", tspToken.getTimeStampInfo().getGenTime());
        LOG.info("AH    : {}", tspToken.getSID().getIssuer());
        LOG.info("Jeton :\n{}", Timestamp.toBase64(tspToken.getEncoded()));
        if (x509 != null) {
            LOG.info("Signataire : {}", x509.getSubjectDN());
        }

        return 0;
    }

    /**
     * Horodatage en ligne de commande.
     */
    public static void main(String[] args) {
        int exitCode = new CommandLine(new TimestampDemo()).execute(args);
        System.exit(exitCode);
    }
}
